# cp-mgmt-export

## Name
This project was created to reduce the manual effort of creating a migrate export across multiple systems in the Lab.  It has been tested with Multi Domain and with Management. The code is the same. 


## Description
The system expects a common backup user, with complex password and shell set to bash.  This is needed to connect to the system and run commands (migrate, scp and so on) 
It also expects access to a user in the Check Point applicaiton, so it can export the file.
Finally, it will expect to send output to a SCP server, for security, and clean up after it has run, so the file created (with name and date/time stamp) doesnt fill up the drive

## Operation
For ease, the playbook is run using bash (backup.sh) but of course it could be used via other tools (AWX). 
It reads the ansible.cfg file, looking for a inventory file (a file telling the system what hosts it needs to work on)
In this example, the inventory file is here: ../Secrets/mgmt-exports.inv.yml 

## Secrets example
The secrets file needs to look like this. In this example, the 1st entry is to show show the variables used, and the second example shows some dummy data. In Ansible, yml formatting is important, so keep the structure. If you want to add more hosts, just copy the section and add host3 for example, and fill in the information needed:
```
Mgmt: 
  hosts:
    <host>:
      ansible_host: <IP-Address_of_CP_Mgmt1>
      ansi_user: <user_with_bash_shell>
      ansi_pass: <complex_pass>
      file_name: <filename_prefix_for_backup>
      ftp_server: <FTP_Server_IP>
      ftp_path: "/home/<folder>/<server_name>"
      ftp_user: <FTP_Acct>
      ftp_pass: <FTP_Pass>
      ftp_file: <same_as_filename_prefix_for_backup>
      cp_ver: <CP_Version>
      file_path: "/var/log/"
      cp_user: <cp_admin_user>
      cp_pass: <cp_admin_pass>
    
    Host2:
      ansible_host: 123.456.789.001
      ansi_user: backup
      ansi_pass: complex_passw0rd
      file_name: host2-backup
      ftp_server: 123.456.789.010
      ftp_path: "/Some/folder/host2-backup"
      ftp_user: ftpbackup
      ftp_pass: ftpbackup-pa44w0rd
      ftp_file: host2-backup
      cp_ver: R81.xx
      file_path: "/var/log/"
      cp_user: admin
      cp_pass: my_cp_pass
```
## Usage
Copy the folder to your system. Ensure the file backup.sh ```chmod 755 backup.sh``` is executable. Create a file ```../Secrets/mgmt-exports.inv.yml``` (exact location can be anywhere, but it is suggested to be outside the folder where the script runs). If you do change the location (and/or name) ensure you also update the inventory location in the ```ansible.cfg``` file.

Running ```./backup.sh``` will read the inventory from ansible.cfg, it will check the configured account shell access exists. Then, run a migrate_server on the system(s).  The system today, will start 10 concurrent tasks, and had been tested with 5 concurrent devices. 

The backup time can vary for each system, so be patient.  It can take a long time for Multi Domain Management servers. The create_export uses https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_async.html to run in the background, and a poller to check when it has completed.  As such, it's normal to see (in /var/log/messages of the CP host being worked upon) a async log message regularly reporting remaining time for the task to complete. 

The poller will continue once the migrate_server finishes on all the CP hosts in tour script. It's normal to see the word "FAILED", while polling, until the process completes. 

Onec the migrate export completes, the tool will copy (using SCP) the file to a different server.  The FTP server IP / credentials can be different per device to backup.  The backup file is created in /var/log in this example, as this should have lots of space. 

Once the SCP begins (async process as dsicussed above for the backup creation) a poller is started to check for completion. 

The poller will continue once the SCP of files from all CP hosts you are working on completes. It's normal to see the word "FAILED", while polling, until the process completes.

The cleanup part of the script, will remove all files that start with the expected file name (as it changes per execution, a specific name cant be used). This will ensure we dont fill up the disk.

## Roadmap
If I can make improvements in the future I will, so anything you want to see added, please just ask.

## License
Shared, as is, no warranty of any kind.

## Project status
Work in progress, when time.
